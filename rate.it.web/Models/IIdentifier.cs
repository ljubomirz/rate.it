﻿using Norm;

namespace rate.it.web.Models
{
    public interface IIdentifier
    {
        ObjectId Id { get; set; } 
    }
}