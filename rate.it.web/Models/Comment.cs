﻿namespace rate.it.web.Models
{
    public class Comment
    {
        public string Message { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}