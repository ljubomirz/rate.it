﻿using System.Text;
using Norm;
using rate.it.web.Helpers;

namespace rate.it.web.Models
{
    public class Overall : IIdentifier
    {
        public Overall()
        {
            Id = ObjectId.NewObjectId();
        }

        public ObjectId Id { get; set; }
        public string Like { get; set; }
		public string Reception { get; set; }
        public string Venue { get; set; }
        public string Refreshment { get; set; }
        public string Lunch { get; set; }
        
        public string Name { get; set; }
        public string Email { get; set; }
        public string EventBriteId { get; set; }
        public string Comment { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendIfValueNotEmpty("Like: ", Like);
            sb.AppendIfValueNotEmpty(", Reception: ", Reception);
            sb.AppendIfValueNotEmpty(", Venue: ", Venue);
            sb.AppendIfValueNotEmpty(", Refreshment: ", Refreshment);
            sb.AppendIfValueNotEmpty(", Lunch: ", Lunch);
            
            sb.AppendIfValueNotEmpty(", Name: ", Name);
            sb.AppendIfValueNotEmpty(", Email: ", Email);
            sb.AppendIfValueNotEmpty(", EventBriteId: ", EventBriteId);
            sb.AppendIfValueNotEmpty(", Comment: ", Comment);

            return sb.ToString();
        }
    }
}