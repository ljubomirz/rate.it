﻿using System;
using System.Configuration;
using System.Linq;
using Norm;

namespace rate.it.web.Models
{
    public class MongoRepository : IRepository
    {
        readonly IMongo _session;

        public MongoRepository()
        {
            var connectionString = ConfigurationManager.AppSettings["MongoDb_Url"];
            _session = Mongo.Create(connectionString, "strict=false");
        }

        #region IRepository Members

        public IQueryable<T> All<T>()
        {
            return _session.GetCollection<T>().AsQueryable();
        }

        public T Single<T>(Func<T, bool> predicate)
        {
            return All<T>().SingleOrDefault(predicate);
        }

        public T Find<T>(Predicate<T> match)
        {
            return All<T>().ToList().Find(match);
        }

        public void Insert<T>(params T[] items)
        {
            _session.GetCollection<T>().Insert(items);
        }

        public void Update<TEntity>(TEntity item) where TEntity : IIdentifier
        {
            _session.GetCollection<TEntity>().UpdateOne(new { _id = item.Id }, item);
        }

        public void Delete<T>(T item)
        {
            _session.GetCollection<T>().Delete(item);
        }

        public void Drop<T>()
        {
            _session.Database.DropCollection(typeof(T).Name);
        }

        #endregion

        public void Dispose()
        {
            _session.Dispose();
        }
    }
}