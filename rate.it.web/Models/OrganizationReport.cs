﻿using System.Collections.Generic;

namespace rate.it.web.Models
{
    public class OrganizationReport
    {
        public Rating Like { get; set; }
        public Rating Reception { get; set; }
        public Rating Venue { get; set; }
        public Rating Refreshments { get; set; }
        public Rating Lunch { get; set; }

        public IList<Comment> Comments { get; set; }
    }
}