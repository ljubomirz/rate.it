﻿using System;

namespace rate.it.web.Models
{
    public class Rating
    {
        public int Poor { get; set; }
        public int Fair { get; set; }
        public int Average { get; set; }
        public int Good { get; set; }
        public int Excelent { get; set; }
        public string Name { get; set; }
        
        public string Overall
        {
            get 
            { 
                var sum = Poor + Fair*2 + Average*3 + Good*4 + Excelent*5;
                
                return String.Format("{0:0.##}", (double)sum / Total);
            }
        }

        public int Total
        {
            get { return Poor + Fair + Average + Good + Excelent; }
        }
    }
}