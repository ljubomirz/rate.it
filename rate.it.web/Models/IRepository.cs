using System;
using System.Linq;

namespace rate.it.web.Models
{
    public interface IRepository : IDisposable
    {
        IQueryable<T> All<T>();

        T Single<T>(Func<T, bool> predicate);
        T Find<T>(Predicate<T> match);

        void Insert<T>(params T[] items);
        void Update<TEntity>(TEntity item) where TEntity : IIdentifier;
        void Delete<T>(T item);

        void Drop<T>();
    } 
}