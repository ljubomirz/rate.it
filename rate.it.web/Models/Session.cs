﻿using System.Text;
using Norm;
using rate.it.web.Helpers;

namespace rate.it.web.Models
{
    public class Session : IIdentifier
    {
        public Session()
        {
            Id = ObjectId.NewObjectId();
        }

        public ObjectId Id { get; set; }
        public string Position { get; set; } 
        public string Track { get; set; } 
        public string Title { get; set; } 
        public string Speaker { get; set; } 
        public string SpeakerImg { get; set; } 
        public string Abstract { get; set; } 
        public string From { get; set; }
		public string To { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendIfValueNotEmpty("Title: ", Title);
            sb.AppendIfValueNotEmpty(", Abstract: ", Abstract);
            sb.AppendIfValueNotEmpty(", Speaker: ", Speaker);
            sb.AppendIfValueNotEmpty(", SpeakerImg: ", SpeakerImg);
            sb.AppendIfValueNotEmpty(", From: ", From);
            sb.AppendIfValueNotEmpty(", To: ", To);
            sb.AppendIfValueNotEmpty(", Track: ", Track);
            sb.AppendIfValueNotEmpty(", Position: ", Position);

            return sb.ToString();
        }
    }
}