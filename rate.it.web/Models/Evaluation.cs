﻿using System.Text;
using Norm;
using rate.it.web.Helpers;

namespace rate.it.web.Models
{
    public class Evaluation : IIdentifier
    {
        public Evaluation()
        {
            Id = ObjectId.NewObjectId();
        }

        public ObjectId Id { get; set; }
        public string Track { get; set; }
		public string SessionId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string EventBriteId { get; set; }
        public string Speaker { get; set; }
        public string Presentation { get; set; }
        public string Comment { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendIfValueNotEmpty("Track: ", Track);
            sb.AppendIfValueNotEmpty(", SessionId: ", SessionId);
            sb.AppendIfValueNotEmpty(", Speaker: ", Speaker);
            sb.AppendIfValueNotEmpty(", Presentation: ", Presentation);
            sb.AppendIfValueNotEmpty(", Comment: ", Comment);
            sb.AppendIfValueNotEmpty(", Name: ", Name);
            sb.AppendIfValueNotEmpty(", Email: ", Email);
            sb.AppendIfValueNotEmpty(", EventBriteId: ", EventBriteId);

            return sb.ToString();
        }
    }
}