﻿using System.Collections.Generic;

namespace rate.it.web.Models
{
    public class EvaluationReport
    {
        public Rating Speaker { get; set; }
        public Rating Presentation { get; set; }

        public IList<Comment> Comments { get; set; }
    }
}