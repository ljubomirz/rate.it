﻿using System.Text;
using Norm;
using rate.it.web.Helpers;

namespace rate.it.web.Models
{
    public class Attendee : IIdentifier
    {
        public Attendee()
        {
            Id = ObjectId.NewObjectId();
        }

        public ObjectId Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string EventBriteId { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendIfValueNotEmpty("FirstName: ", FirstName);
            sb.AppendIfValueNotEmpty(", LastName: ", LastName);
            sb.AppendIfValueNotEmpty(", Email: ", Email);
            sb.AppendIfValueNotEmpty(", EventBriteId: ", EventBriteId);

            return sb.ToString();
        }

        public string FullName { get { return FirstName + " " + LastName; } }
    }

    public class EligibleAttendee : Attendee {}
    
    public class Winner : Attendee {}


}