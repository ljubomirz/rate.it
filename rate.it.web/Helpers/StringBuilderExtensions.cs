﻿using System.Text;

namespace rate.it.web.Helpers
{
    public static class StringBuilderExtensions
    {
        public static void AppendIfValueNotEmpty(this StringBuilder sb, string key, string value)
        {
            if (!string.IsNullOrEmpty(value.Trim()))
            {
                sb.Append(key);
                sb.AppendLine(value);
            }
        } 
    }
}