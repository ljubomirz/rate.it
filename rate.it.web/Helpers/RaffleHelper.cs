﻿using System;
using System.Collections.Generic;

namespace rate.it.web.Helpers
{
    public static class RaffleHelper
    {

        public static string GetLuckyWinner(IList<string> eligibleAttendeeIds)
        {
            var luckyId = "";

            if (eligibleAttendeeIds != null)
            {
                if (eligibleAttendeeIds.Count > 0)
                {
                    var random = new Random();
                    int randomIndex = random.Next(0, eligibleAttendeeIds.Count);

                    luckyId = eligibleAttendeeIds[randomIndex];
                }
            }

            return luckyId;
        }
        
    }
}