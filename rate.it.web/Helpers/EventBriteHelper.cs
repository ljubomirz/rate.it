﻿using System.Configuration;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using rate.it.web.Models;
using System.Collections.Generic;
using System;

namespace rate.it.web.Helpers
{
    public static class EventBriteHelper
    {
        private const string PAGE_SIZE = "5000";
        private const string EMAIL_REGEX = @"^(?("")(""[^""]+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" + 
              @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$";
        private const string NOT_CHECKED_IN = "unused";

        public static IList<Attendee> GetAttendeeList()
        {
            var attendeeList = new List<Attendee>();

            string eventBriteApiKey = ConfigurationManager.AppSettings["eb_appApiKey"];
            string eventBriteUserKey = ConfigurationManager.AppSettings["eb_userApiKey"];
            string eventBriteEventId = ConfigurationManager.AppSettings["eb_eventId"];
            var  wClient = new WebClient();
            var result = wClient.OpenRead(new Uri("https://www.eventbrite.com/xml/event_list_attendees?app_key=" + eventBriteApiKey +
                "&user_key=" + eventBriteUserKey + "&id=" + eventBriteEventId + "&count=" + PAGE_SIZE 
                + "&show_full_barcodes=true&do_not_display=profile%2Canswers%2Caddress&page=1"));

            var xDoc = XDocument.Load(result);
            var xAtendees = xDoc.Root;

            if (xAtendees != null)
                foreach (var xAtendee in xAtendees.Elements("attendee"))
                {
                    var attendee = new Attendee();

                    attendee.EventBriteId = xAtendee.Element("id").Value;
                    attendee.FirstName = xAtendee.Element("first_name").Value;
                    attendee.LastName = xAtendee.Element("last_name").Value;
                    attendee.Email = xAtendee.Element("email").Value;

                    var firstBarcode = xAtendee.Element("barcodes").Elements().FirstOrDefault();
                
                    var status = firstBarcode != null ? firstBarcode.Element("status").Value : "unused";

                    if (!string.IsNullOrWhiteSpace(status) && !NOT_CHECKED_IN.Equals(status))
                        attendeeList.Add(attendee);
                }

            return attendeeList;
        }

        public static IList<EligibleAttendee> GetEligibleList(IRepository repository)
        {
            var evaluationEmails = repository.All<Evaluation>().Select(ev => ev.Email).ToList();
            var overallEmails = repository.All<Overall>().Select(ov => ov.Email).ToList();
            evaluationEmails.AddRange(overallEmails);

            var eligibleEmails = evaluationEmails.Distinct().ToList();
            var emailRegEx = new Regex(EMAIL_REGEX);

            var validEligibleEmails = new List<string>();
            foreach (var eligibleEmail in eligibleEmails)
            {
               if(eligibleEmail != null && emailRegEx.IsMatch(eligibleEmail))
                   validEligibleEmails.Add(eligibleEmail);
            }


            var attendeeList = repository.All<Attendee>().ToList();

            var eligibleAttendees = from attendee in attendeeList
                                    join validEmail in validEligibleEmails on attendee.Email equals validEmail
                                    select new EligibleAttendee {
                                                FirstName = attendee.FirstName,
                                                LastName = attendee.LastName,
                                                Email = attendee.Email,
                                                EventBriteId = attendee.EventBriteId
                                               };
            

            return eligibleAttendees.Distinct().ToList();
        }
        
    }
}