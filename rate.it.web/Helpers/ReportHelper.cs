﻿using System.Collections.Generic;
using System.Linq;
using rate.it.web.Models;

namespace rate.it.web.Helpers
{
    public class ReportHelper
    {
        public static EvaluationReport CalculateEvaluationReport(IList<Evaluation> evaluations, bool showNames)
        {
            var report = new EvaluationReport();
            var speakerRates = evaluations.GroupBy(ev => ev.Speaker).Select(s => new { Value = s.Key, Count = s.Count() }).ToDictionary(s => s.Value, s => s.Count);
            var presentationRates = evaluations.GroupBy(ev => ev.Presentation).Select(s => new { Value = s.Key, Count = s.Count() }).ToDictionary(s => s.Value, s => s.Count);

            var comments =
                evaluations.Where(ev => !string.IsNullOrWhiteSpace(ev.Comment)).Select(
                c => new Comment {Message = c.Comment, Email = showNames ? c.Email : "", Name = showNames ? c.Name : ""}).ToList();

            report.Speaker = GetRatingFor(speakerRates, "Speaker");
            report.Presentation = GetRatingFor(presentationRates, "Presentation");
            report.Comments = comments;

            return report;
        }
        

        public static OrganizationReport CalculateOrganizationReport(IList<Overall> overalls, bool showNames)
        {
            var report = new OrganizationReport();

            var likeRates = overalls.GroupBy(ov => ov.Like).Select(s => new { Value = s.Key, Count = s.Count() }).ToDictionary(s => s.Value, s => s.Count);
            var receptionRates = overalls.GroupBy(ov => ov.Reception).Select(s => new { Value = s.Key, Count = s.Count() }).ToDictionary(s => s.Value, s => s.Count);
            var venueRates = overalls.GroupBy(ov => ov.Venue).Select(s => new { Value = s.Key, Count = s.Count() }).ToDictionary(s => s.Value, s => s.Count);
            var refreshmentsRates = overalls.GroupBy(ov => ov.Refreshment).Select(s => new { Value = s.Key, Count = s.Count() }).ToDictionary(s => s.Value, s => s.Count);
            var lunchRates = overalls.GroupBy(ov => ov.Lunch).Select(s => new { Value = s.Key, Count = s.Count() }).ToDictionary(s => s.Value, s => s.Count);

            var comments =
                overalls.Where(ev => !string.IsNullOrWhiteSpace(ev.Comment)).Select(
                    c => new Comment { Message = c.Comment, Email = showNames ? c.Email : "", Name = showNames ? c.Name : "" }).ToList();

            report.Like = GetRatingFor(likeRates, "Likes");
            report.Reception = GetRatingFor(receptionRates, "Reception");
            report.Venue = GetRatingFor(venueRates, "Venue");
            report.Refreshments = GetRatingFor(refreshmentsRates, "Refreshments");
            report.Lunch = GetRatingFor(lunchRates, "Lunch");
            report.Comments = comments;
            
            return report;
        }


        private static Rating GetRatingFor(IDictionary<string, int> groupResult, string groupName)
        {
            var rating = new Rating
            {
                Poor = groupResult.ContainsKey("1") ? groupResult["1"] : 0,
                Fair = groupResult.ContainsKey("2") ? groupResult["2"] : 0,
                Average = groupResult.ContainsKey("3") ? groupResult["3"] : 0,
                Good = groupResult.ContainsKey("4") ? groupResult["4"] : 0,
                Excelent = groupResult.ContainsKey("5") ? groupResult["5"] : 0,
                Name = groupName
            };

            return rating;
        }
    }
}