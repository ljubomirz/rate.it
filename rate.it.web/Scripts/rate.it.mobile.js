﻿var viewModel = {
    // Data
    tracks: ['red', 'blue', 'yellow', 'green', 'purple'],
    selectedTrack: ko.observable(),
    selectedSessionId: ko.observable(),
    userName: ko.observable(),
    trackSessions: ko.observableArray([]),
    showAjaxLoader: ko.observable(false),

    // Behaviours
    selectTrack: function (track) {
        this.selectedSessionId(null);
        this.selectedTrack(track);
    },

    selectSession: function (sessionId) {
        this.selectedSessionId(sessionId);
        $('#slider_0').val('3'); $('#slider_1').val('3');
        $('#slider_0').slider('refresh'); $('#slider_1').slider('refresh');
        $('#comment').val('');
    },

    submitEvaluation: function (formElement) {
        this.showAjaxLoader(true);
        var evaluation = {
            "Name": formElement.name.value,
            "Email": formElement.email.value,
            "EventBriteId": formElement.eventbriteid.value,
            "Track": this.selectedTrack(),
            "SessionId": this.selectedSessionId(),
            "Speaker": formElement.speaker.value,
            "Presentation": formElement.presentation.value,
            "Comment": formElement.comment.value
        };
        $.post("/eval/submit", evaluation, function (response) {
            viewModel.showAjaxLoader(false);

            var userName = response.length ? response : "attendee";
            viewModel.userName(userName);

            $.mobile.changePage($("#thx"), { changeHash: false });
        });
    },

    resetView: function () {
        this.selectedTrack(null);
        this.selectedSessionId(null);
    }
};

viewModel.selectedSession = ko.dependentObservable(function () {
    var sessionIdToFind = this.selectedSessionId();

    return ko.utils.arrayFirst(viewModel.trackSessions(), function (item) { return item.Id == sessionIdToFind });
}, viewModel);

//ko.dependentObservable(function () {
//    var sessionId = this.selectedSessionId();
//    var trackId = this.selectedTrack();

//    if (!trackId) {
//        //$("#scroll").animate({ marginLeft: "-20px" }, 1000);
//        //$.mobile.changePage($("#tracks"), { changeHash: false });
//    } else {
//        if (!sessionId) {
//            //$("#scroll").animate({ marginLeft: "-820px" }, 700);
//            $.mobile.changePage($("#sessions"), { changeHash: false });
//        } else {
//            //$("#scroll").animate({ marginLeft: "-1620px" }, 700);
//            $.mobile.changePage($("#form"), { changeHash: false });
//        }
//    }
//}, viewModel);

ko.bindingHandlers.refreshListView = {

    update: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());

        if (value.length)
            $(element).listview("refresh");
    }
};

window.rateViewModel = viewModel;
ko.applyBindings(viewModel);

ko.dependentObservable(function () {
    //if (this.sessionsRequest) this.sessionsRequest.abort(); // Prevent concurrent requests
        
    if(this.selectedTrack())
        // Get track sessions data from JSON web service and feed output into trackSessions
        this.sessionsRequest = $.get("/eval/sessions", { track: this.selectedTrack() }, this.trackSessions);
}, viewModel);

//ko.linkObservableToUrl(viewModel.selectedTrack, "track");
//ko.linkObservableToUrl(viewModel.selectedSessionId, "session" /* hash param name */);

function updateRatingText(element) {
    var value = element.val();
    var text = "Average";

    switch (value) {
        case "1": text = "Poor"; break;
        case "2": text = "Fair"; break;
        case "3": text = "Average"; break;
        case "4": text = "Good"; break;
        case "5": text = "Excellent"; break;
        default:
            break;
    }

    element.parent().find('label span').text(text);
}