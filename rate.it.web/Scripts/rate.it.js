﻿var viewModel = {
    // Data
    tracks: ['red', 'blue', 'yellow', 'green', 'purple'],
    selectedTrack: ko.observable(),
    selectedSessionId: ko.observable(),
    selectedType: ko.observable(),
    userName: ko.observable(),
    trackSessions: ko.observableArray([]),
    showAjaxLoader: ko.observable(false),
    showBack: ko.observable(false),
    showNext: ko.observable(false),

    // Behaviours
    selectType: function (type) {
        this.selectedType(type);
    },

    selectTrack: function (track) {
        this.selectedSessionId(null);
        this.selectedTrack(track);
    },

    selectSession: function (sessionId) {
        this.selectedSessionId(sessionId);
    },

    submitEvaluation: function (formElement) {
        this.showAjaxLoader(true);
        var evaluation = {
            "Name": formElement.name.value,
            "Email": formElement.email.value,
            "EventBriteId": formElement.eventbriteid.value,
            "Track": this.selectedTrack(),
            "SessionId": this.selectedSessionId(),
            "Speaker": $(formElement.speaker).filter(':checked').val(),
            "Presentation": $(formElement.presentation).filter(':checked').val(),
            "Comment": formElement.comment.value
        };
        $.post("/eval/submit", evaluation, function (response) {
            viewModel.showAjaxLoader(false);
            viewModel.showBack(false);
            
            $("#scroll").delay(1000).animate({ marginLeft: "-4820px" }, 700);
            var userName = response.length ? response : "attendee";
            viewModel.userName(userName);
        });
    },

    submitOverall: function (formElement) {
        this.showAjaxLoader(true);
        var evaluation = {
            "Name": formElement.name.value,
            "Email": formElement.email.value,
            "EventBriteId": formElement.eventbriteid.value,
            "Like": $(formElement.like).filter(':checked').val(),
            "Reception": $(formElement.reception).filter(':checked').val(),
            "Venue": $(formElement.venue).filter(':checked').val(),
            "Refreshment": $(formElement.refreshment).filter(':checked').val(),
            "Lunch": $(formElement.lunch).filter(':checked').val(),
            "Comment": formElement.comment.value
        };
        $.post("/eval/submitoverall", evaluation, function (response) {
            viewModel.showAjaxLoader(false);
            viewModel.showNext(false);
            $("#scroll").delay(1000).animate({ marginLeft: "-20px" }, 700);
            var userName = response.length ? response : "attendee";
            viewModel.userName(userName);
        });
    },

    resetView: function () {
        this.selectedType(null);
        this.selectedTrack(null);
        this.selectedSessionId(null);
    }
};

viewModel.selectedSession = ko.dependentObservable(function () {
    var sessionIdToFind = this.selectedSessionId();
        
    return ko.utils.arrayFirst(viewModel.trackSessions(), function (item) { return item.Id == sessionIdToFind });
}, viewModel);

ko.dependentObservable(function () {
    var type = this.selectedType();
    var sessionId = this.selectedSessionId();
    var trackId = this.selectedTrack();

    if (!type) {
        $("#scroll").animate({ marginLeft: "-1620px" }, 1000);
        this.showNext(false);
        this.showBack(false);
    } else {
        if (type == "overall") {
            $("#scroll").animate({ marginLeft: "-820px" }, 1000);
            this.showNext(true);
        } else if (type == "presentation") {
            this.showBack(true);
            if (!trackId) {
                $("#scroll").animate({ marginLeft: "-2420px" }, 1000);
            } else {
                if (!sessionId) {
                    $("#scroll").animate({ marginLeft: "-3220px" }, 700);
                } else {
                    $("#scroll").animate({ marginLeft: "-4020px" }, 700);
                }
            }
        }
    }
}, viewModel);

ko.bindingHandlers.refreshListView = {
    update: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());

        if (value.length)
            $(element).listview("refresh");
    }
};

window.rateViewModel = viewModel;
ko.applyBindings(viewModel);

ko.dependentObservable(function () {
    if (this.sessionsRequest) this.sessionsRequest.abort(); // Prevent concurrent requests
        
    if(this.selectedTrack())
        // Get track sessions data from JSON web service and feed output into trackSessions
        this.sessionsRequest = $.get("/eval/sessions", { track: this.selectedTrack() }, this.trackSessions);
}, viewModel);


ko.linkObservableToUrl(viewModel.selectedType, "type");
ko.linkObservableToUrl(viewModel.selectedTrack, "track");
ko.linkObservableToUrl(viewModel.selectedSessionId, "session" /* hash param name */);