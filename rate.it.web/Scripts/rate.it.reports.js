﻿var viewModel = {
    // Data
    tracks: ['red', 'blue', 'yellow', 'green', 'purple'],
    selectedTrack: ko.observable(),
    selectedSessionId: ko.observable(),
    selectedType: ko.observable(),
    userName: ko.observable(),
    trackSessions: ko.observableArray([]),
    showBack: ko.observable(false),
    showNext: ko.observable(false),
    overallData: ko.observable(),
    presentationData: ko.observable(),

    // Behaviours
    selectType: function (type) {
        this.selectedType(type);
    },

    selectTrack: function (track) {
        this.selectedSessionId(null);
        this.selectedTrack(track);
    },

    selectSession: function (sessionId) {
        this.selectedSessionId(sessionId);
    },

    resetView: function () {
        this.selectedType(null);
        this.selectedTrack(null);
        this.selectedSessionId(null);
    },

    createChart: function (rating, container) {
        $(container).html("test");
        //        var options = {
        //            chart: {
        //                renderTo: container,
        //                plotBackgroundColor: null,
        //                plotBorderWidth: null,
        //                plotShadow: false
        //            },
        //            title: {
        //                text: rating
        //            },
        //            tooltip: {
        //                formatter: function () {
        //                    return '<b>' + this.point.name + '</b>: ' + this.percentage + ' %';
        //                }
        //            },
        //            plotOptions: {
        //                pie: {
        //                    allowPointSelect: true,
        //                    cursor: 'pointer',
        //                    dataLabels: {
        //                        enabled: true,
        //                        color: Highcharts.theme.textColor || '#000000',
        //                        connectorColor: Highcharts.theme.textColor || '#000000',
        //                        formatter: function () {
        //                            return '<b>' + this.point.name + '</b>: ' + this.percentage + ' %';
        //                        }
        //                    }
        //                }
        //            },
        //            series: [{
        //                type: 'pie',
        //                name: rating,
        //                data: [
        //                ['Poor', rating.Poor],
        //                ['Fair', rating.Fair],
        //                ['Average', rating.Average],
        //                ['Good', rating.Good],
        //                {
        //                    name: 'Excelent',
        //                    y: rating.Excelent,
        //                    sliced: true,
        //                    selected: true
        //                }
        //             ]
        //            }]
        //        };

        //        var chart = new Highcharts.Chart(options);
        //        return chart;
    }
};

ko.dependentObservable(function () {
    var type = this.selectedType();
    var sessionId = this.selectedSessionId();
    var trackId = this.selectedTrack();

    if (!type) {
        $("#scroll").animate({ marginLeft: "-820px" }, 1000);
        this.showNext(false);
        this.showBack(false);
    } else {
        if (type == "overall") {
            $("#scroll").animate({ marginLeft: "-20px" }, 1000);
            this.showNext(true);
        } else if (type == "presentation") {
            this.showBack(true);
            if (!trackId) {
                $("#scroll").animate({ marginLeft: "-1620px" }, 1000);
            } else {
                if (!sessionId) {
                    $("#scroll").animate({ marginLeft: "-2420px" }, 700);
                } else {
                    $("#scroll").animate({ marginLeft: "-3220px" }, 700);
                }
            }
        }
    }
}, viewModel);

viewModel.selectedSession = ko.dependentObservable(function () {
    var sessionIdToFind = this.selectedSessionId();

    return ko.utils.arrayFirst(viewModel.trackSessions(), function (item) { return item.Id == sessionIdToFind; });
}, viewModel);

ko.bindingHandlers.chart = {
    update: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());

        if (value && value.length)
            createChart(value, $(element));
    }
};

window.reportViewModel = viewModel;
ko.applyBindings(viewModel);

ko.dependentObservable(function () {
    if (this.sessionsRequest) this.sessionsRequest.abort(); // Prevent concurrent requests
        
    if(this.selectedTrack())
        // Get track sessions data from JSON web service and feed output into trackSessions
        this.sessionsRequest = $.get("/eval/sessions", { track: this.selectedTrack() }, this.trackSessions);
}, viewModel);

ko.dependentObservable(function () {
    if (this.overallRequest) this.overallRequest.abort(); // Prevent concurrent requests

    if (this.selectedType() == "overall")
        // Get overall data
        this.overallRequest = $.get("/eval/overall", {}, this.overallData);
}, viewModel);

ko.dependentObservable(function () {
    if (this.presentationRequest) this.presentationRequest.abort(); // Prevent concurrent requests

    if (this.selectedSessionId())
        // Get overall data
        this.presentationRequest = $.get("/eval/presentation", { track: this.selectedTrack(), id: this.selectedSessionId() }, this.presentationData);
}, viewModel);


ko.linkObservableToUrl(viewModel.selectedType, "type");
ko.linkObservableToUrl(viewModel.selectedTrack, "track");
ko.linkObservableToUrl(viewModel.selectedSessionId, "session" /* hash param name */);





