﻿using System.Web.Mvc;
using System.Web.Routing;
using Castle.Windsor;
using Castle.Windsor.Installer;

namespace rate.it.web
{
    public class MvcApplication : System.Web.HttpApplication
    {
		
		public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Report", // Route name
                "report/", // URL with parameters
                new { controller = "Eval", action = "Report", id = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Eval", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);

            InitializeContainer();
            InitializeControllerFactory();
        }

        protected void InitializeContainer()
        {
            if (_container == null)
            {
                _container = new WindsorContainer().Install(FromAssembly.This());
            }
        }

        protected void InitializeControllerFactory()
        {
            var controllerFactory = _container.Resolve<IControllerFactory>();
            ControllerBuilder.Current.SetControllerFactory(controllerFactory);
        }

        static IWindsorContainer _container;
        public IWindsorContainer Container
        {
            get { return _container; }
        }
    }

}