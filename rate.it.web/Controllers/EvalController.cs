﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using rate.it.web.Helpers;
using rate.it.web.Models;

namespace rate.it.web.Controllers
{
    public class EvalController : Controller
    {
		private IRepository _repository;
        private const string SPEAKER = "speaker";
		
		public EvalController(IRepository repository)
		{
		    _repository = repository;
		}


        public ActionResult Index()
        {
            return View();
        }
        
        [Authorize]
        public ActionResult Report()
        {
            return View();
        }

        public JsonResult Sessions(string track)
        {
        	var sessions = _repository.All<Session>().Where(s => s.Track == track).OrderBy(s => s.Position);

			List<object> jsonFriendly = new List<object>();

			foreach (var session in sessions)
			{
				jsonFriendly.Add(new
				                 	{
				                 		Track = session.Track,
										Id = session.Position,
										Title = session.Title,
										Speaker = session.Speaker,
										SpeakerImg = session.SpeakerImg,
										Abstract = session.Abstract,
										From = session.From,
										To = session.To
				                 	}
				); 
			}

        	return Json(jsonFriendly, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Submit(Evaluation evaluation)
        {
			_repository.Insert(evaluation);

            return Json(string.IsNullOrWhiteSpace(evaluation.Name) ? "attendee" : evaluation.Name);
        }

        [HttpPost]
        public JsonResult SubmitOverall(Overall overall)
        {
            _repository.Insert(overall);

            return Json(string.IsNullOrWhiteSpace(overall.Name) ? "attendee" : overall.Name);
        }

        [Authorize]
        public JsonResult Presentation(string id, string track)
        {
            var presentationEvaluations =
                _repository.All<Evaluation>().Where(e => e.Track == track && e.SessionId == id).ToList();
            bool canSeeNames = !SPEAKER.Equals(User.Identity.Name);
            // calculate statistics...
            var evaluationReport = ReportHelper.CalculateEvaluationReport(presentationEvaluations, canSeeNames);

            return Json(evaluationReport, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public JsonResult Overall()
        {
            var overalls = _repository.All<Overall>().ToList();
            bool canSeeNames = !SPEAKER.Equals(User.Identity.Name);
            // calculate statistics...
            var overallReport = ReportHelper.CalculateOrganizationReport(overalls, canSeeNames);

            return Json(overallReport, JsonRequestBehavior.AllowGet);
        }
    }
}
