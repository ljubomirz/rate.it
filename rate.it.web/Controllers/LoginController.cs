﻿using System.Collections.Generic;
using System.Configuration;
using System.Web.Mvc;
using System.Web.Security;
using rate.it.web.Models;

namespace rate.it.web.Controllers
{
    public class LoginController : Controller
    {
        private List<string> _validUsers = new List<string> {"admin", "speaker", "organizer"};


        public ActionResult Index()
        {
            return View();
        }

		[HttpPost]
		public ActionResult Index(Login login)
		{
			if (ModelState.IsValid && _validUsers.Contains(login.Username))
			{
                var password = ConfigurationManager.AppSettings[login.Username];

				if (!string.IsNullOrWhiteSpace(password) && password.Equals(login.Password))
				{
                    if (!string.IsNullOrWhiteSpace(Request.QueryString["ReturnUrl"]))
                    {
                        FormsAuthentication.RedirectFromLoginPage(login.Username, true);
                    }
				    else
                    {
                        FormsAuthentication.SetAuthCookie(login.Username, true);
                        return RedirectToAction("index", "admin", null);    
                    }
				    
				}
			}

			return View(login);
		}

		public ActionResult Logout()
		{
			FormsAuthentication.SignOut();

			return RedirectToAction("Index");
		}
    }
}
