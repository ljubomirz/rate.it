﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using rate.it.web.Helpers;
using rate.it.web.Models;

namespace rate.it.web.Controllers
{
    [Authorize(Users = "admin")]
    public class AdminController : Controller
    {
		private readonly IRepository _repository;
		
		public AdminController(IRepository repository)
		{
		    _repository = repository;
		}

        
        public ActionResult Index()
		{
			var sessionTracks = _repository.All<Session>().Select(s => s.Track).ToArray();
		    var tracks = sessionTracks.Distinct();

            var attendeeCount = _repository.All<Attendee>().ToList().Count;
            var eligibleCount = _repository.All<EligibleAttendee>().ToList().Count;
            var evaluationsCount = _repository.All<Evaluation>().ToList().Count;
            var overallsCount = _repository.All<Overall>().ToList().Count;
            var winnersCount = _repository.All<Winner>().ToList().Count;

            ViewBag.Attendees = attendeeCount;
            ViewBag.Evaluations = evaluationsCount;
            ViewBag.Eligibles = eligibleCount;
            ViewBag.Overalls = overallsCount;
            ViewBag.Winners = winnersCount;


            return View(tracks.ToList());
		}

        #region Manage Tracks
        public ActionResult Sessions(string id)
		{
		    string track = id;
			List<Session> sessions;

			if (String.IsNullOrWhiteSpace(track))
			{
				sessions = _repository.All<Session>().ToList();
			}
			else
			{
                sessions = _repository.All<Session>().Where(s => s.Track == track).ToList();
			}
            
            return View(sessions);
		}

		public ActionResult Session(string id)
		{
			Session info = new Session();

			if (!string.IsNullOrWhiteSpace(id))
			{
               info = _repository.All<Session>().Single(s => s.Id == id);
			}

			return View(info);
		}

		[HttpPost]
		public ActionResult Session(Session info)
		{
			if (ModelState.IsValid)
			{
			    var existingData = _repository.All<Session>().Where(s => s.Id == info.Id).FirstOrDefault();

                if(existingData != null)
                {
                    _repository.Update(info);    
                }
                else
                {
                    _repository.Insert(info);
                }

				return RedirectToAction("Sessions");
			}

			return View(info);
		}
        
    	public ActionResult DeleteSession(string id)
		{
            if (!string.IsNullOrWhiteSpace(id))
            {
                var info = _repository.All<Session>().Where(s => s.Id == id).SingleOrDefault();
                if(info != null)
                    _repository.Delete(info);
            }
			
			return RedirectToAction("Index");
		}
        #endregion 

        #region Data Cleanup
        [HttpPost]
		public ActionResult ClearEvaluations()
		{
			_repository.Drop<Evaluation>();

			return RedirectToAction("Index");
		}

        [HttpPost]
		public ActionResult ClearOveralls()
		{
            _repository.Drop<Overall>();

			return RedirectToAction("Index");
		}
        
        [HttpPost]
		public ActionResult ClearWinners()
		{
            _repository.Drop<Winner>();

			return RedirectToAction("Index");
		}
        #endregion 

        #region Raffle
        [HttpPost]
        public ActionResult GetAttendeeList()
        {
            var attendeeList = EventBriteHelper.GetAttendeeList();
            
            if(attendeeList.Count > 0)
            {
                var currentList = _repository.All<Attendee>().ToList();
                if(currentList.Count > 0) 
                    _repository.Drop<Attendee>();

                _repository.Insert(attendeeList.ToArray());
            }

            return RedirectToAction("Index");
        }
        
        [HttpPost]
        public ActionResult GetEligibleList()
        {
            var eligibleList = EventBriteHelper.GetEligibleList(_repository);

            if (eligibleList.Count > 0)
            {
                var currentList = _repository.All<EligibleAttendee>().ToList();
                if (currentList.Count > 0)
                    _repository.Drop<EligibleAttendee>();

                _repository.Insert(eligibleList.ToArray());
            }

            return RedirectToAction("Index");
        }
        #endregion    
    }
}
