﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using rate.it.web.Helpers;
using rate.it.web.Models;

namespace rate.it.web.Controllers
{
    [Authorize]
    public class RaffleController : Controller
    {
        private IRepository _repository;
		
		public RaffleController(IRepository repository)
		{
		    _repository = repository;
		}


        public ActionResult Index()
        {
            return View();
        }

        public JsonResult SelectWinner()
        {
            var eligibleIdList = GetListOfEligibleAtteendees();

            string luckyWinnerId = RaffleHelper.GetLuckyWinner(eligibleIdList);

            eligibleIdList.Remove(luckyWinnerId);

            Attendee luckyAttendee = _repository.All<EligibleAttendee>().Where(e => e.EventBriteId == luckyWinnerId).FirstOrDefault();
            var luckyWinner = new Winner();
            if (luckyAttendee != null)
            {
                luckyWinner = new Winner
                                      {
                                          FirstName = luckyAttendee.FirstName,
                                          LastName = luckyAttendee.LastName,
                                          Email = luckyAttendee.Email,
                                          EventBriteId = luckyAttendee.EventBriteId
                                      };

                _repository.Insert(luckyWinner);

                Session["eligibleListIds"] = eligibleIdList;
            }

            return Json(luckyWinner.FullName, JsonRequestBehavior.AllowGet);
        }

        private IList<string> GetListOfEligibleAtteendees()
        {
            var eligibleIds = (IList<string>)Session["eligibleListIds"];

            if (eligibleIds == null)
            {
                eligibleIds = _repository.All<EligibleAttendee>().Select(e => e.EventBriteId).ToList();
                var winnerIds = _repository.All<Winner>().Select(e => e.EventBriteId).ToList();

                foreach (var winnerId in winnerIds)
                {
                    if (eligibleIds.Contains(winnerId))
                        eligibleIds.Remove(winnerId);
                }
            }

            return eligibleIds;
        }
    }
}
